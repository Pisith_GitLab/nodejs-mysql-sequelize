const Sequelize = require('sequelize');
const AuthorModel = require('./models/author');
const BookModel = require('./models/book');

const sequelize = new Sequelize('demo', 'root', "root", {
    host: 'localhost',
    dialect: 'mysql',
    port: 3306,
    pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
})

const Book = BookModel(sequelize, Sequelize);
const Author = AuthorModel(sequelize, Sequelize);

// Relationship => Author has many to book
Author.hasMany(Book);

sequelize.sync({ force: false })
.then(() => {
    console.log('Database and Tables created here')
})

module.exports = {
    Author,
    Book
}