const express = require('express');
const bodyParser = require('body-parser');
const { Author, Book } = require('./sequelize');

const app = express();

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.json({message : 'Welcome to our API'})
});

// create a author
app.post('/api/author', (req, res) => {
    Author.create(req.body)
    .then(author => {
        res.json(author);
    })
});

// create a book
app.post('/api/book', (req, res) => {
    Book.create(req.body)
    .then(book => {
        res.json(book);
    })
});

// get all books
app.get('/api/books', (req, res) => {
    Book.findAll().then(books => {
        res.json(books);
    })
});

app.get('/api/authors', (req, res) => {
    Author.findAll().then(authors => {
        res.json(authors);
    })
})

const port = 3000;
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});